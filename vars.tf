# Заменить на token своего образа
# Token можно узнать с помощью команды yc config list 
variable "yc_token" {
   default = ""
}
# Заменить на ID своего облака
# ID можно узнать с помощью команды yc config list
variable "yc_cloud_id" {
  default = "b1gjto7m1imcroim1idm"
}
# Заменить на ID своего фолдера
# ID можно узнать с помощью команды yc config list
variable "yc_folder_id" {
  default = "b1g1kckip0s6fh9085gq"
}
# Заменить на ID своего region
# ID можно узнать с помощью команды yc config list
variable "yc_region" {
  default = "ru-central1-a"
}
# Заменить на ID своего образа
# ID можно узнать с помощью команды yc compute image list
# Public images can be lookup by CLI yc compute image list --folder-id standard-images
variable "ubuntu-20.04" {
  default = "fd87tirk5i8vitv9uuo1"
}

