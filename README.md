yc CLI comands
```
https://cloud.yandex.com/en/docs/cli/cli-ref/
```
Command Usage Syntax: yc <group|command>

CLI manage
```
yc init — CLI initialization
yc version — Display Yandex Cloud CLI version.
yc help — Help provides help for any command in the application. Simply type yc help [path to command] for full details.
yc config — The 'yc config' command group lets you set, view and unset properties used by Yandex Cloud CLI.
yc components — Manage installed components
```
Operations manage
```
yc operation — manage operations
yc operation get — Get operation
yc operation wait — Wait for operation to complete
Service manage
yc iam — Manage Yandex Identity and Access Manager resources
yc resource-manager — Manage Yandex Resource Manager resources
yc compute — Manage Yandex Compute Cloud resources
yc vpc — Manage Yandex Virtual Private Cloud resources
yc dns — Manage Yandex DNS resources
yc managed-kubernetes — Manage Kubernetes clusters.
yc ydb — Manage YDB databases.
yc kms — Manage Yandex Key Management Service resources
yc cdn — Manage CDN resources
yc certificate-manager — Manage Certificate Manager resources
yc managed-clickhouse — Manage ClickHouse clusters, hosts, databases, backups, users and ml-models.
yc managed-mongodb — Manage MongoDB clusters, hosts, databases, backups and users.
yc managed-mysql — Manage MySQL clusters, hosts, databases, backups and users.
yc managed-sqlserver — Manage SQLServer clusters, databases, backups and users.
yc managed-postgresql — Manage PostgreSQL clusters, hosts, databases, backups and users.
yc managed-redis — Manage Redis clusters, hosts, databases, backups and users.
yc managed-elasticsearch — Manage ElasticSearch clusters, hosts, indexes and backups.
yc managed-kafka — Manage Apache Kafka clusters, brokers, topics and users.
yc container — Manage Container resources.
yc load-balancer — Manage Yandex Load Balancer resources
yc datatransfer — Manage Data Transfer endpoints and transfers
yc serverless — Manage Serverless resources.
yc iot — Manage Yandex IoT Core resources
yc dataproc — Manage data processing clusters.
yc application-load-balancer — [PREVIEW] Manage Yandex Application Load Balancer resources
yc logging — [PREVIEW] Manage Yandex Cloud Logging
yc lockbox — [PREVIEW] Manage Yandex Lockbox resources
yc organization-manager — Manage Yandex Organization Manager resources
yc cloud-desktop — Manage cloud desktop resources.
```
