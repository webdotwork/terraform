# Provider
provider "yandex" {
  token     = "${var.yc_token}"
  cloud_id  = "${var.yc_cloud_id}"
  folder_id = "${var.yc_folder_id}"
  zone      = "${var.yc_region}"
}

# Set compute image variable
data "yandex_compute_image" "ubuntu" {
  family = "ubuntu-2004-lts"
}

# VPC network
resource "yandex_vpc_network" "dvps" {
  name = "dvpsnet"
}
# VPC subnet
resource "yandex_vpc_subnet" "subnet" {
  name           = "subnet"
  v4_cidr_blocks = ["10.10.10.0/24"]
  zone           = "${var.yc_region}"
  network_id     = "${yandex_vpc_network.dvps.id}"
}
# Resources VM
resource "yandex_compute_instance" "dvps" {
  name = "dvps"
  hostname    = "dvps.local"
  platform_id = "standard-v1"
  
  resources {
    cores  = 2
    memory = 2
    core_fraction = 100
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu.id
      type     = "network-hdd"
      size     = "20"

    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet.id
    nat       = true
    ipv6      = false
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}

